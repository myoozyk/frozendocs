<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!-- lifted from troff+man by doclifter -->
<refentry id='bzadmin6'>
<refmeta>
<refentrytitle>bzadmin</refentrytitle>
<manvolnum>6</manvolnum>
</refmeta>
<refnamediv id='name'>
<refname>bzadmin</refname>
<refpurpose>a text based client for BZFlag</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
<cmdsynopsis>
  <command>bzadmin</command>

    <arg choice='opt'>-help </arg>
    <arg choice='opt'><arg choice='plain'>-ui </arg><group choice='opt'><arg choice='plain'><replaceable>curses</replaceable></arg><arg choice='plain'><replaceable>stdboth</replaceable></arg><arg choice='plain'><replaceable>stdin</replaceable></arg><arg choice='plain'><replaceable>stdout</replaceable></arg></group></arg>
    <arg choice='plain'><replaceable>callsign@hostname:</replaceable></arg>
    <arg choice='opt'><replaceable>port</replaceable></arg>
    <arg choice='opt'><replaceable>command</replaceable></arg>
    <arg choice='opt' rep='repeat'><replaceable>command</replaceable></arg>

</cmdsynopsis>
</refsynopsisdiv>


<refsect1 id='description'><title>DESCRIPTION</title>
<para><command>bzadmin</command>
is a textbased client for the game BZFlag. It can't be used for
playing, but it can be used to see when players join and leave the
game, to see the chat messages, and to send messages and commands
to the server.</para>

<para>When you start bzadmin without any command line options other than
callsign and hostname a simple curses-based user interface will be
started (unless you built bzadmin without curses support). This
interface is divided into three rectangles; the output window
(which covers almost all of the terminal), the target window, and
the input window.</para>

<para>The output window is where messages from the server will be shown.</para>

<para>The target window shows the name of the player that
will receive your next message, or 'all' if it will be a public message.
You can change target by using the left and right arrow keys.</para>

<para>The input window is where you type your messages. It also supports tab
completion of commands and callsigns. You can clear the input window
with the key combination <emphasis remap='B'>Ctrl-U</emphasis>, and you can generate a <filename>/kick</filename>
command for the current target with the F5 key (if the current target
is a player).</para>

<refsect2 id='options'><title>Options</title>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><option>-help</option></term>
<listitem>
<para>Show a simple help text.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-ui </option>{<emphasis remap='I'>curses</emphasis> | <emphasis remap='I'>stdboth</emphasis> | <emphasis remap='I'>stdin</emphasis> | <emphasis remap='I'>stdout</emphasis>}</term>
<listitem>
<para>Select the user interface that you want. The curses interface is the default,
and it is described above.
<!-- .br -->
The stdin interface reads user commands from the standard in stream
and sends them to the server. All server output is ignored.
<!-- .br -->
The stdout interface prints all server output to the standard out stream.
All user input is ignored.
<!-- .br -->
The stdboth interface is a combination of stdin and stdout - it prints
server output to the standard out stream, and reads user commands from
the standard in stream.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='I'>callsign@hostname</emphasis>[<emphasis remap='I'>:port</emphasis>]</term>
<listitem>
<para>Specifies the callsign that you want your client to use, and the
hostname where the BZFlag server is. The port number is optional,
and the default value is 5154.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='I'>command</emphasis></term>
<listitem>
<para>You can specify messages and commands to send to the server on the
command line. If any such commands are specified, bzadmin will
exit as soon as those commands has been sent, without starting
an user interface.</para>
<!-- .RE -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect2>

<refsect2 id='examples'><title>Examples</title>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><emphasis remap='B'>bzadmin admin@localhost:5154</emphasis></term>
<listitem>
<para>Join the game on localhost, port 5154, using the callsign 'admin'.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>bzadmin admin@localhost '/passwd secretpass' '/ban 192.168.0.2'</emphasis></term>
<listitem>
<para>Connect to the server at localhost and ban the IP 192.168.0.2.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><userinput>bzadmin -ui stdout spy@bzserver.xy | grep magicword</userinput></term>
<listitem>
<para>Connect to bzserver.xy and print all server messages that contain 'magicword'.</para>
<!-- .RE -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect2>
</refsect1>

<refsect1 id='see_also'><title>SEE ALSO</title>
<para><citerefentry><refentrytitle>bzfs</refentrytitle><manvolnum>6</manvolnum></citerefentry>, <citerefentry><refentrytitle>bzflag</refentrytitle><manvolnum>6</manvolnum></citerefentry></para>
</refsect1>
</refentry>

